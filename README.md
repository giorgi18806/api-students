Test Task (PHP/Laravel)

Stack Requirements:
- PHP ^7.4
- Laravel ^7.22
- MySQL ^8.0

Description:
Make storage for school students and their grades with API and without interface. 

API Methods to realize:
/api/students
  - return all students list; should receive filters search, class, faculty, teacher and age; should receive operators limit, offset, sort_by and sort_order;  returns attributes { name, class, birthday, avg_grade, faculty, teacher }

- /api/students/add - add a new student; should receive attributes { name, class, birthday, faculty, teacher }

- /api/grades/{student_id}/get - return all attributes for one student and the list of his latest grades - { subject, grade, teacher, date };  should receive operators limit and offset (affects to grades list); should receive filters subject, teacher; 

- /api/grades/{student_id}/add - add a new grade for specified student; should receive attributes { subject, grade, teacher }

- grade is decimal [0.00, 5.00]

- age is integer

- birthday is datetime

- other attributes is string (60)

Table structures for developer desire

Realization Requirements:

— Authorization by static Bearer token

— POST requests only

— JSON/form-data format for requests

— JSON format for answers


