<?php

namespace Database\Factories;

use App\Models\Grade;
use App\Models\Student;
use Illuminate\Database\Eloquent\Factories\Factory;

class GradeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Grade::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'student_id' => function(){
                return Student::all()->random();
            },
            'subject' => function(){
                $subject = $this->faker->randomElements($array = array ('Maths','Physics','Chemistry', 'History'), $count = 1);
                return $subject[0];
            },
            'grade' => $this->faker->biasedNumberBetween(1, 12),
            'teacher' => $this->faker->name,
        ];
    }
}
