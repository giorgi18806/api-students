<?php

namespace Database\Factories;

use App\Models\Student;
use Illuminate\Database\Eloquent\Factories\Factory;

class StudentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Student::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->lastName,
            'birthday' => $this->faker->dateTimeBetween($startDate = '-17 years', $endDate = '-6 years', $timezone = null),
            'class' => $this->faker->numberBetween(1, 12),
            'faculty' => $this->faker->word,
            'teacher' => $this->faker->name,
        ];
    }
}
