<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\Integer;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class Student extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function grade()
    {
        return $this->hasMany(Grade::class);
    }

    public static function filtering()
    {
        return QueryBuilder::for(Student::class)
            ->allowedFilters(['name', 'class', 'faculty', 'teacher', AllowedFilter::scope('ofAge')])
            ->allowedSorts(['name'])
            ->get();
    }

    public function scopeOfAge($query, $age)
    {
        return $query->whereYear('birthday', '=', now()->subYears($age));
    }
}
