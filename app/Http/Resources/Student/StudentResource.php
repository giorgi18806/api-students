<?php

namespace App\Http\Resources\Student;

use App\Http\Resources\GradeResource;
use Illuminate\Http\Resources\Json\JsonResource;

class StudentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'name' => $this->name,
            'class' => $this->class,
            'birthday' => $this->birthday,
            'grades' => $this->grade->count() > 0 ? GradeResource::collection($this->grade) : "No grades yet",
            'faculty' => $this->faculty,
            'teacher' => $this->teacher,
        ];
    }
}
