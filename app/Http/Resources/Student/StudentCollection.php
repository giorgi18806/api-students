<?php

namespace App\Http\Resources\Student;

use Illuminate\Http\Resources\Json\JsonResource;

class StudentCollection extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name' => $this->name,
            'class' => $this->class,
            'birthday' => $this->birthday,
            'avg_grade' => $this->grade->count() > 0 ? round($this->grade->sum('grade')/
                $this->grade->count(), 2) : 'No grades yet',
            'faculty' => $this->faculty,
            'teacher' => $this->teacher,
        ];
    }
}
