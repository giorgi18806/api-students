<?php

namespace App\Http\Controllers;

use App\Http\Requests\GradeRequest;
use App\Http\Requests\StudentRequest;
use App\Http\Resources\Student\StudentCollection;
use App\Http\Resources\Student\StudentResource;
use App\Http\Resources\GradeResource;
use App\Models\Grade;
use App\Models\Student;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Http\Response;
use Ramsey\Uuid\Type\Integer;
use Spatie\QueryBuilder\QueryBuilder;

class StudentController extends Controller
{
    /**
     * StudentController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the students.
     *
     * @return AnonymousResourceCollection
     */
    public function index()
    {
        $result = Student::filtering();
        return StudentCollection::collection($result);
    }

    /**
     * Store a newly created student in storage.
     *
     * @param  StudentRequest  $request
     * @return Response
     */
    public function store(StudentRequest $request)
    {
        $student = Student::create($request->input());
        return response([
            'data' => new StudentResource($student)
        ], Response::HTTP_CREATED);
    }

    /**
     * Display the specified student.
     *
     * @param Student $student_id
     * @return StudentResource
     */
    public function show(Student $student_id)
    {
        return new StudentResource($student_id);
    }

    /**
     * Store a newly created grade for the specified student in storage.
     *
     * @param Student $student_id
     * @param GradeRequest $request
     * @return Application|ResponseFactory|Response
     */
    public function grade(Student $student_id, GradeRequest $request)
    {
        $request['student_id'] = $student_id->id;
        $grade = Grade::create($request->input());
        return response([
            'data' => new GradeResource($grade)
        ], Response::HTTP_CREATED);
    }
}
