<?php

use App\Models\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Spatie\QueryBuilder\QueryBuilder;


Route::middleware('auth:api')->post('/user', function (Request $request) {
    return $request->user();
});

Route::post('students', [App\Http\Controllers\StudentController::class, 'index'])->name('students.index');
Route::post('/students/add', [App\Http\Controllers\StudentController::class, 'store'])->name('students.store');

Route::group(['prefix' => 'grades'], function() {
    Route::post('/{student_id}/get', [App\Http\Controllers\StudentController::class, 'show'])->name('grades.index');
    Route::post('/{student_id}/add', [App\Http\Controllers\StudentController::class, 'grade'])->name('grades.store');
});
